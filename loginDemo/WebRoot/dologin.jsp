<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=utf-8"%>
	<!-- 处理登录的页面不需要任何的HTML代码，它只是业务逻辑 -->
<%
	request.setCharacterEncoding("utf-8");//设置request对象的编码，防止中文乱码
    String username = request.getParameter("username");
    String password = request.getParameter("password");
   
    if("admin".equals(username)&&"admin".equals(password)){
        /* 将用户的数据保存到session中 */
    	session.setAttribute("username", username);
    	session.setAttribute("password", password);
    	request.getRequestDispatcher("login_success.jsp").forward(request, response);
    }else{
    	response.sendRedirect("login_failure.jsp");
    }
%>