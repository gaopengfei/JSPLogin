<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>请登录</title>
<link type="text/css" href="css/login.css" rel="stylesheet" />
</head>

<body>
	<div id="container">
		<div class="logo">
			<img src="assets/logo.png" />
		</div>
		<div id="content">
			<form action="dologin.jsp" method="post" name="loginForm">
				<p class="main">
					<label>用户名：</label>
					<input type="text" name="username" />
					<label>密码：</label>
					<input type="password" name="password" />
				</p>
				<p class="space">	
					<input type="submit" value="提交" class="login" style="cursor: pointer;"/>
				</p>
			</form>
		</div>
	</div>
</body>
</html>
