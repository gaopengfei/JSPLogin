<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>登录失败！</title>
<link type="text/css" href="css/login.css" rel="stylesheet" />
</head>

<body>
	<div id="container">
		<div class="logo">
			<img src="assets/logo.png" />
		</div>
		<div id="content">
			<h1>登录失败，错误的用户名或者密码！</h1>
			<a href="login.jsp">点击此处返回登录页面重新登录</a>
		</div>
	</div>
</body>
</html>
